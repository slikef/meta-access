SUMMARY = "The canonical example of init scripts"
SECTION = "base"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${S}/COPYING;md5=886557d0c9eee76bfbb292c1e01e2f43"

SRC_URI = "git://github.com/zerotier/ZeroTierOne.git;protocol=https;branch=master \
	file://0001-add-armv7-a-architecture.patch \
"
SRCREV = "133b64679c5423a20c407ad8a5d713a58ced38e1"

PR = "r1"

S = "${WORKDIR}/git"

EXTRA_OEMAKE = "'CFLAGS=${CFLAGS}' 'BUILDDIR=${S}'"

do_install () {
	install -d ${D}${sbindir}
	install -d ${D}${sysconfdir}/init.d
	install -d ${D}${sysconfdir}/rc5.d

	install -m 0755 ${S}/zerotier-one ${D}${sbindir}/
	ln -s zerotier-one ${D}${sbindir}/zerotier-cli
	ln -s zerotier-one ${D}${sbindir}/zerotier-idtool
}

INHIBIT_PACKAGE_STRIP = "1"
INSANE_SKIP:${PN} = "ldflags"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
PACKAGES = "${PN}"
